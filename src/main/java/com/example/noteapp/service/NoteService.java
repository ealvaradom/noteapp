package com.example.noteapp.service;

import com.example.noteapp.Entity.Note;
import com.example.noteapp.model.NoteDto;
import com.example.noteapp.repository.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class NoteService {

    @Autowired
    private NoteRepository noteRepository;

    @Autowired
    private NoteModelMapping noteModelMapping;

    public NoteDto createNote(NoteDto noteDto) {
        Note note = new Note();

        note = noteModelMapping.toEntity(noteDto);

        return noteModelMapping.toDto(noteRepository.save(note));
    }

    public NoteDto getNoteByUserId(Long userId) {
        Optional<Note> optionalNote = noteRepository.findById(userId);
        return noteModelMapping.toDto(optionalNote.get());
    }
}
