package com.example.noteapp.service;

import com.example.noteapp.Entity.User;
import com.example.noteapp.model.UserDto;
import org.springframework.stereotype.Service;

@Service
public class UserModelMapping{
    public User toEntity (UserDto userDto){

        User user =  new User();
        user.setId(userDto.id());
        user.setUserName(userDto.userName());
        user.setPassword(userDto.password());

        return user;
    }

    public UserDto toDto (User user){
        return new UserDto(user.getId(), user.getUserName(), user.getPassword());
    }
}
