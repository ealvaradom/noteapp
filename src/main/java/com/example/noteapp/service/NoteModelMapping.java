package com.example.noteapp.service;

import com.example.noteapp.Entity.Note;
import com.example.noteapp.model.NoteDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NoteModelMapping {

    @Autowired
    private UserModelMapping userModelMapping;
    public Note toEntity(NoteDto noteDto){
        Note note = new Note();

        note.setId(noteDto.id());
        note.setNoteDetail(noteDto.noteDetail());
        note.setNoteName(noteDto.noteName());
        note.setUser(userModelMapping.toEntity(noteDto.user()));

        return note;
    }

    public NoteDto toDto(Note note){
        return new NoteDto(note.getId(), userModelMapping.toDto(note.getUser()),
                note.getNoteName(), note.getNoteDetail());
    }
}
