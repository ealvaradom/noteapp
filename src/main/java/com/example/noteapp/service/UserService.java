package com.example.noteapp.service;

import com.example.noteapp.Entity.User;
import com.example.noteapp.model.UserDto;
import com.example.noteapp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserModelMapping userModelMapping;

    public UserDto createUser(UserDto userDto) {
        User user = userModelMapping.toEntity(userDto);
        return userModelMapping.toDto(userRepository.save(user));
    }

    public UserDto getUserByUserId(Long userId) {
        Optional<User> optionalUser = userRepository.findById(userId);
        return userModelMapping.toDto(optionalUser.get());
    }
}
