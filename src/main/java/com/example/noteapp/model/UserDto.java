package com.example.noteapp.model;

public record UserDto (Long id, String userName, String password){
}
