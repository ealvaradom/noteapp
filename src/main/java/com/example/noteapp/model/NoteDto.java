package com.example.noteapp.model;

public record NoteDto(Long id, UserDto user, String noteName, String noteDetail) {
}
