package com.example.noteapp.controller;

import com.example.noteapp.model.NoteDto;
import com.example.noteapp.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/note")
public class NoteController {

    @Autowired
    private NoteService noteService;

    @PostMapping("/save")
    public ResponseEntity<String> createNote(@RequestBody NoteDto note) {
        noteService.createNote(note);
        return new ResponseEntity<>("Request successfully processed", HttpStatus.OK);

    }

    @GetMapping("/{userId}")
    public NoteDto getNoteByUserId(@PathVariable Long userId) {
        return noteService.getNoteByUserId(userId);
    }

}
