package com.example.noteapp.controller;

import com.example.noteapp.model.UserDto;
import com.example.noteapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/save")
    public ResponseEntity<String> createNote(@RequestBody UserDto user) {
        userService.createUser(user);
        return new ResponseEntity<>("Request successfully processed", HttpStatus.OK);

    }

    @GetMapping("/{userId}")
    public UserDto getUserByUserId(@PathVariable Long userId) {
        return userService.getUserByUserId(userId);
    }
}
